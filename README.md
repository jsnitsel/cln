# What is cln

This is a short, simple rust program that was created to quickly generate the cacheline for
a given physical address while I was helping someone debug a dma issue.

# Installation

Prerequisites: A rust installation and cargo

To install simply run the following command from the base directory of the repository:

```
cargo install --path=.
```

# Usage

```
cln 0.1.0
Translate physical addresses to page frame nuber, page offset, and cacheline

USAGE:
    cln [FLAGS] <paddr>

FLAGS:
    -l, --largepage    system with large 64k pages
    -h, --help         Prints help information
    -V, --version      Prints version information

ARGS:
    <paddr>    physical address to parse
```

# License

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation; either version 2 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.
