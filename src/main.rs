// SPDX-License-Identifier: GPL-2.0-or-later
// SPDX-FileCopyrightText: 2023 Jerry Snitselaar <jsnitsel@redhat.com>

use clap::Parser;
use std::num::ParseIntError;

fn parse_hex(src: &str) -> Result<usize, ParseIntError> {
    usize::from_str_radix(src, 16)
}

#[derive(Parser)]
#[command(
    version,
    about = "Translate physical addresses to page frame number, page offset, and cacheline",
    long_about = None
)]
struct Cli {
    #[arg(short = 'p', long = "large-page", help = "system with 64k pages")]
    arm64: bool,

    #[arg(
        short = 'c',
        long = "large-cacheline",
        help = "system with 128 Byte cacheline"
    )]
    ppc64: bool,

    #[arg(value_parser = parse_hex, help = "physical address to parse")]
    paddr: usize,
}

/*
 * l1 cache shift: 64 Byte cachline = 6 bits
 * page shift: 12 bits for 4k pages, 16 bits for 64k page
 * page mask: 0xfff for 4k page, 0xffff for 64k page
 */
const L1_CACHE_SHIFT: u8 = 6;
const PPC_L1_CACHE_SHIFT: u8 = 7;
const PAGE_SHIFT: u8 = 12;
const PAGE_MASK: usize = 0xfff;
const ARM_64K_PAGE_MASK: usize = 0xffff;
const ARM_64K_PAGE_SHIFT: u8 = 16;

fn main() {
    let args = Cli::parse();
    let pfn: usize;
    let offset: usize;
    let cln: usize;
    let paddr = args.paddr;

    /*
     * cacheline = (page-frame-number << (page-shift - l1-cache-shift)) + (page-offset >> l1-cache-shift)
     */
    if args.arm64 {
        pfn = paddr >> ARM_64K_PAGE_SHIFT;
        offset = paddr & ARM_64K_PAGE_MASK;
        cln = (pfn << (ARM_64K_PAGE_SHIFT - L1_CACHE_SHIFT)) + (offset >> L1_CACHE_SHIFT);
    } else if args.ppc64 {
        pfn = paddr >> PAGE_SHIFT;
        offset = paddr & PAGE_MASK;
        cln = (pfn << (PAGE_SHIFT - PPC_L1_CACHE_SHIFT)) + (offset >> PPC_L1_CACHE_SHIFT);
    } else {
        pfn = paddr >> PAGE_SHIFT;
        offset = paddr & PAGE_MASK;
        cln = (pfn << (PAGE_SHIFT - L1_CACHE_SHIFT)) + (offset >> L1_CACHE_SHIFT);
    }

    println!(
        "paddr={:x} pfn={:x} offset={:x} cln={:x}",
        paddr, pfn, offset, cln
    );
}
